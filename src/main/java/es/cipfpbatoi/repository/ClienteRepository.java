package es.cipfpbatoi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import es.cipfpbatoi.modelo.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, Integer>  {


}

//public interface ClienteRepository extends CrudRepository<Cliente, Integer>{
//
//}
