package es.cipfpbatoi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.cipfpbatoi.modelo.Cliente;
import es.cipfpbatoi.repository.ClienteRepository;
import jakarta.transaction.Transactional;

@Service
@Transactional
public class ClienteService {
	@Autowired
	private ClienteRepository clienteRespository;	

	public ClienteService() {
	}

	public List<Cliente> listAllClientes() {
		return clienteRespository.findAll();
	}

	public Cliente getCliente(Integer id) {
		return clienteRespository.findById(id).get();
	}

	public Cliente saveCliente(Cliente cli) {
		return clienteRespository.save(cli);
	}

	public void deleteCliente(Integer id) {
		clienteRespository.deleteById(id);
	}

}
